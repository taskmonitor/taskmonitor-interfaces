# TaskMonitor Interfaces

Protobuf interfaces for TaskMonitor

## Getting started

This project is used as submodule by TaskMonitor and TaskMonitor Collector (client) as IPC interface specification.
Other clients can be implemented using these interfaces for IPC.
